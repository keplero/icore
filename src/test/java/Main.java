import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.util.Utils;
import ac.github.icore.internal.yaml.BukkitYamls;
import ac.github.icore.internal.yaml.annotation.Yaml;
import ac.github.icore.internal.yaml.annotation.YamlList;
import ac.github.icore.internal.yaml.annotation.YamlMap;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream("test.yml");
        assert resourceAsStream != null;
        InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(inputStreamReader);

        try {
            DemoBean demoBean = BukkitYamls.inject(new DemoBean(), configuration);
            System.out.println("demoBean.setting = " + demoBean.setting);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        //        try {
//            DemoBean bean = BukkitYamls.inject(new DemoBean(), configuration);
//            System.out.println("bean = " + bean.list);
//        } catch (IllegalAccessException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
    }

    public static class DemoBean {
        @YamlMap(name = "install.level-setting", value = Integer.class)
        public Map<String, Integer> setting = new HashMap<>();

    }


}
