package ac.github.icore.internal.event;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.lang.reflect.Field;

public class EventAdapter extends Event implements Trigger {

    public static HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    private void autoAsync() {
        try {
            Field field = Event.class.getDeclaredField("async");
            field.setAccessible(true);
            field.set(this, !Bukkit.isPrimaryThread());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void call() {
        autoAsync();
        Bukkit.getServer().getPluginManager().callEvent(this);
    }
}
