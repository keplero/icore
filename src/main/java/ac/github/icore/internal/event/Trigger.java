package ac.github.icore.internal.event;

public interface Trigger {

    void call();
}
