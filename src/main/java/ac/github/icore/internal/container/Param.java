package ac.github.icore.internal.container;

import ac.github.icore.internal.conversion.Conversions;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Param extends HashMap<String, Object> {

    public Param add(String key, Object value) {
        put(key, value);
        return this;
    }

    public static Param of(Map<String, Object> objectMap) {
        Param param = Param.newInstance();
        for (Entry<String, Object> entry : objectMap.entrySet()) {
            if (entry.getValue() instanceof MemorySection) {
                param.add(entry.getKey(), Param.of(((MemorySection) entry.getValue()).getValues(false)));
            } else {
                param.add(entry.getKey(), entry.getValue());
            }

        }
        return param;
    }

    public static Param of(ConfigurationSection config) {
        Param param = of(config.getValues(false));
        param.add("====configName", config.getName());
        return param;
    }

    public Map<String, Object> map() {
        Map<String, Object> map = new HashMap<>();
        this.forEach((key, value) -> {
            if (value instanceof Param) {
                map.put(key, ((Param) value).map());
            } else if (value instanceof MemorySection) {
                map.put(key, Param.of(((MemorySection) value).getValues(false)).map());
            } else {
                map.put(key, value);
            }
        });
        return map;
    }

    public String getString(String key) {
        return getString(key, null);
    }

    public String getString(String key, String def) {
        return containsKey(key) ? String.valueOf(replacePlaceholder(String.valueOf(get(key)))) : def;
    }

    public String replacePlaceholder(String string) {

        Pattern compile = Pattern.compile("\\{[^}]+}");
        Matcher matcher = compile.matcher(string);
        while (matcher.find()) {
            String key = matcher.group().replaceAll("[{|}]", "");
            if (containsKey(key)) {
                string = string.replace("{" + key + "}", getString(key));
            }
        }

        return string;
    }


    public int getInt(String key) {
        return Integer.parseInt(getString(key));
    }


    public int getInt(String key, int def) {
        return Integer.parseInt(getString(key, String.valueOf(def)));
    }


    public double getDouble(String key) {
        return Double.parseDouble(getString(key));
    }

    public long getLong(String key) {
        return getLong(key, 0);
    }

    public long getLong(String key, long def) {
        return Long.parseLong(getString(key, String.valueOf(def)));
    }


    public double getDouble(String key, double def) {
        return Double.parseDouble(getString(key, "0"));
    }

    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(getString(key));
    }


    public boolean getBoolean(String key, boolean def) {
        return Boolean.parseBoolean(getString(key, "false"));
    }

    @Override
    public Object get(Object key) {
        String string = key.toString();
        String[] split = string.split("\\.");
        if (split.length != 1) {
            String substring = string.substring(split[0].length() + 1);
            return getParam(split[0]).get(substring);
        } else {
            return super.get(key);
        }
    }

    public Param getParam(String key) {
        return (Param) get(key);
    }

    public Param push(Param param) {
        for (Entry<String, Object> entry : param.entrySet()) {
            add(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public <E> List<E> getList(String key, Class<?> c) {
        List<?> list = (List<?>) get(key);
        List<E> eList = new ArrayList<>();
        for (Object o : list) {
            eList.add(Conversions.covert(o, c));
        }
        return eList;
    }

    public static Param newInstance() {
        return new Param();
    }

}
