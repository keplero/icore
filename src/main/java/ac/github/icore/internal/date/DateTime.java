package ac.github.icore.internal.date;

import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.util.Utils;

public class DateTime {

    private long current;
    private long date;
    private Param param = Param.newInstance();

    public DateTime(long current, long date) {
        this.current = current;
        this.date = date;
    }

    public Param eval(){

        long l = (date - current) / 1000;
        long hour = 0;
        long minute = 0;
        long seconds = 0;
        if (l > 0) {
            hour = l / 3600;
            minute = (l - hour * 3600) / 60;
            seconds = l - hour * 3600 - minute * 60;
        }
        param
                .add("HH",hour)
                .add("mm", minute)
                .add("ss",seconds);
        return param;
    }

    public String from(String string){
        return Utils.replace(string,eval());
    }

}
