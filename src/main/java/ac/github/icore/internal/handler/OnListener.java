package ac.github.icore.internal.handler;

import ac.github.icore.internal.chest.Chest;
import ac.github.icore.internal.chest.ClickEvent;
import ac.github.icore.internal.service.PackageRegistry;
import ac.github.icore.internal.service.Service;
import ac.github.icore.internal.service.annotation.Auto;
import ac.github.icore.internal.service.bean.JavaClassBean;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;

public class OnListener implements Listener {


    @EventHandler
    void e(InventoryClickEvent e) {
        Chest.find(e, false).ifPresent(chest -> chest.clicks.forEach(clickEventConsumer -> clickEventConsumer
                .accept(new ClickEvent(chest, e))));
    }

    @EventHandler
    void e(InventoryCloseEvent e) {
        Chest.find(e, false).ifPresent(chest -> {
            Chest.chests.remove(chest);

            chest.closes.forEach(inventoryCloseEventConsumer -> inventoryCloseEventConsumer.accept(e));
        });
    }

    @EventHandler
    void e(PluginEnableEvent e) {
        Plugin plugin = e.getPlugin();

        if (plugin.getClass().isAnnotationPresent(Auto.class)) {
            new PackageRegistry(plugin);
        }

    }

    @EventHandler
    void e(PluginDisableEvent e) {
        Plugin plugin = e.getPlugin();
        Iterator<Map.Entry<String, JavaClassBean>> iterator = Service.javaClassBeanMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, JavaClassBean> next = iterator.next();
            JavaClassBean value = next.getValue();
            if (value.parent.equals(plugin)) {
                iterator.remove();
            }
        }
    }
}