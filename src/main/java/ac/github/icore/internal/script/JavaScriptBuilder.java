package ac.github.icore.internal.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class JavaScriptBuilder {

    private ScriptEngine scriptEngine;

    public JavaScriptBuilder() {
        scriptEngine = new ScriptEngineManager().getEngineByName("JavaScript");
    }

    public JavaScriptBuilder putObject(String key,Object value){
        scriptEngine.put(key,value);
        return this;
    }

    public Object eval(String string) throws ScriptException {
        return scriptEngine.eval(string);
    }
}
