package ac.github.icore.internal.item;

import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.util.Utils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ItemBuilder {

    public ConfigurationSection section;

    public String material;
    public String name;
    public int data;
    public List<String> lore;
    public int amount = 1;

    public ItemBuilder() {
    }

    public ItemBuilder(ConfigurationSection section) {
        this.section = section;
    }

    public ItemBuilder material(String material) {
        this.material = material;
        return this;
    }


    public ItemBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder data(int data) {
        this.data = data;
        return this;
    }

    public ItemBuilder amount(int amount) {
        this.amount = amount;
        return this;
    }


    public ItemBuilder lore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder lore(String string) {
        if (lore == null) {
            lore = new ArrayList<>();
        }
        lore.add(string);
        return this;
    }

    public ItemStack build() {
        ItemStack item = Utils.item(name, lore, material);
        item.setAmount(amount);
        item.setDurability((short) data);
        return item;
    }


    public ItemStack buildConfig(Param param) {
        YamlConfiguration configuration = new YamlConfiguration();
        for (String key : section.getKeys(false)) {
            if (section.isList(key)) {
                List<String> stringList = section.getStringList(key);
                configuration.set(key, Utils.replaceList(stringList, param));
            } else {
                configuration.set(key, Utils.replace(section.getString(key), param));
            }
        }
        return Utils.item(configuration, param);
    }
}
