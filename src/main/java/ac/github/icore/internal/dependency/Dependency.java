package ac.github.icore.internal.dependency;

import ac.github.icore.internal.util.FileUtils;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Dependency {

    public static Map<String, String> internalMap = new HashMap<>();

    static {
        internalMap.put("mybatis", "https://repo1.maven.org/maven2/org/mybatis/mybatis/3.5.6/mybatis-3.5.6.jar");
        internalMap.put("slf4j", "https://repo1.maven.org/maven2/org/slf4j/slf4j-api/1.7.30/slf4j-api-1.7.30.jar");
        internalMap.put("hikari-cp", "https://repo1.maven.org/maven2/com/zaxxer/HikariCP/3.4.5/HikariCP-3.4.5.jar");
        internalMap.put("kotlin-reflect", "https://repo1.maven.org/maven2/org/jetbrains/kotlin/kotlin-reflect/1.4.30-RC/kotlin-reflect-1.4.30-RC.jar");
        internalMap.put("lucene", "https://repo1.maven.org/maven2/org/apache/lucene/lucene-core/8.7.0/lucene-core-8.7.0.jar");
        internalMap.put("kotlin-stdlib", "https://repo1.maven.org/maven2/org/jetbrains/kotlin/kotlin-stdlib/1.4.0/kotlin-stdlib-1.4.0.jar");
        internalMap.put("kotlin-stdlib8", "https://repo1.maven.org/maven2/org/jetbrains/kotlin/kotlin-stdlib-jdk8/1.4.0/kotlin-stdlib-jdk8-1.4.0.jar");
        internalMap.put("kotlin-stdlib7", "https://repo1.maven.org/maven2/org/jetbrains/kotlin/kotlin-stdlib-jdk7/1.4.0/kotlin-stdlib-jdk7-1.4.0.jar");
    }

    public static File library = new File("./library");
    public static List<String> caches = new ArrayList<>();
    private String[] urls;
    public List<File> files = new ArrayList<>();

    public Dependency() {
    }

    public Dependency internal(String... keys) {
        List<String> list = Arrays.asList(keys);
        Object[] objects = internalMap.entrySet().stream().filter(stringStringEntry -> list.contains(stringStringEntry.getKey())).map(Map.Entry::getValue).toArray();
        urls = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            urls[i] = objects[i].toString();
        }
        return this;
    }

    public Dependency urls(String... urls) {
        this.urls = urls;
        return this;
    }

    public Dependency(String... urls) {
        this.urls = urls;
    }


    public Dependency afterDownload() {

        if (!library.exists()) {
            library.mkdirs();
        }

        for (String url : urls) {
            String[] split = url.split("/");
            String file_name = split[split.length - 1];
            File file = new File(library, file_name);
            if (!file.exists()) {
                try {
                    Bukkit.getConsoleSender().sendMessage("[Dependency] start download " + url + ".");
                    final long download = FileUtils.download(url, file);
                    Bukkit.getConsoleSender().sendMessage("[Dependency] Download " + url + " successful,Time consuming " + download + "ms.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (file.exists()) {
                files.add(file);
            }
        }
        return this;
    }

    public void injectAll() {
        files.forEach(this::injectFile);
    }

    public void injectFile(File file) {

        if (file.isDirectory()) {
            Arrays.stream(file.listFiles()).forEach(this::injectFile);
        } else {
            if (file.getName().endsWith(".jar") && !caches.contains(file.getName())) {
                try {
                    FileUtils.addUrl(file);
                    caches.add(file.getName());
                    Bukkit.getConsoleSender().sendMessage("[Dependency] Inject library jar " + file.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }


}
