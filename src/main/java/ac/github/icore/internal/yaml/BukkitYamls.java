package ac.github.icore.internal.yaml;

import ac.github.icore.ICore;
import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.conversion.Conversions;
import ac.github.icore.internal.service.JavaBeans;
import ac.github.icore.internal.yaml.annotation.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.*;

public class BukkitYamls {


    public static <T> T inject(T instance, ConfigurationSection configuration) throws IllegalAccessException, InvocationTargetException {
        return inject(instance, Param.of(configuration));
    }


    public static <T> T inject(T instance, Param param) throws IllegalAccessException, InvocationTargetException {
        Class<?> aClass = instance.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            if (declaredField.isAnnotationPresent(Yaml.class)) {
                Yaml yaml = declaredField.getAnnotation(Yaml.class);
                Object o = param.get(path0(yaml.value(), declaredField));
                declaredField.set(instance, o);
            } else if (declaredField.isAnnotationPresent(YamlObject.class)) {
                YamlObject yamlObject = declaredField.getAnnotation(YamlObject.class);
                Object target = null;
                try {
                    target = Objects.requireNonNull(JavaBeans.newInstance(yamlObject.clazz().equals(Void.class) ? declaredField.getType() : yamlObject.clazz()));
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
                declaredField.set(instance, inject(target, param.getParam(path0(yamlObject.value(), declaredField))));
            } else if (declaredField.isAnnotationPresent(YamlMap.class)) {
                YamlMap yamlMap = declaredField.getAnnotation(YamlMap.class);
                Class<?> keyClass = yamlMap.key();
                Class<?> valueClass = yamlMap.value().equals(Void.class) ? declaredField.getType() : yamlMap.value();
                Map<Object, Object> map = (Map<Object, Object>) declaredField.get(instance);
                for (String key : param.getParam(yamlMap.name()).keySet()) {
                    String pathKey = yamlMap.name() + "." + key;
                    Object covert;
                    try {
                        covert = inject(Objects.requireNonNull(JavaBeans.newInstance(valueClass)), param.getParam(pathKey));
                    } catch (ClassCastException | NullPointerException | NoSuchFieldException e) {
                        covert = Conversions.covert(param.get(pathKey), valueClass);
                    }
                    map.put(Conversions.covert(key, keyClass), covert);
                }
            } else if (declaredField.isAnnotationPresent(YamlList.class)) {
                YamlList yamlList = declaredField.getAnnotation(YamlList.class);
                Class<?> valueClass = yamlList.value().equals(Void.class) ? declaredField.getType() : yamlList.value();
                List<Object> list = (List<Object>) declaredField.get(instance);
                list.addAll(param.getList(yamlList.name(), valueClass));
            } else if (declaredField.isAnnotationPresent(YamlKey.class)) {
                declaredField.set(instance, param.getString("====configName"));
            }
        }

        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            declaredMethod.setAccessible(true);
            if (declaredMethod.isAnnotationPresent(Yaml.class)) {
                Yaml yaml = declaredMethod.getAnnotation(Yaml.class);
                Object o = param.get(path0(yaml.value(), declaredMethod));
                declaredMethod.invoke(instance, o);
            } else if (declaredMethod.isAnnotationPresent(YamlObject.class)) {
                YamlObject yamlObject = declaredMethod.getAnnotation(YamlObject.class);
                Object target = null;
                try {
                    target = Objects.requireNonNull(JavaBeans.newInstance(yamlObject.clazz()));
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
                declaredMethod.invoke(instance, inject(target, param.getParam(path0(yamlObject.value(), declaredMethod))));
            } else if (declaredMethod.isAnnotationPresent(YamlMap.class)) {
                YamlMap yamlMap = declaredMethod.getAnnotation(YamlMap.class);
                Class<?> keyClass = yamlMap.key();
                Class<?> valueClass = yamlMap.value();
                Map<Object, Object> map = new HashMap<>();
                for (String key : param.getParam(yamlMap.name()).keySet()) {
                    String pathKey = yamlMap.name() + "." + key;
                    Object covert = Conversions.covert(param.get(pathKey), valueClass);
                    if (covert == null) {
                        try {
                            covert = inject(Objects.requireNonNull(JavaBeans.newInstance(valueClass)), param.getParam(pathKey));
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        }
                    }
                    map.put(Conversions.covert(key, keyClass), covert);
                }
                declaredMethod.invoke(instance, map);
            } else if (declaredMethod.isAnnotationPresent(YamlList.class)) {
                YamlList yamlList = declaredMethod.getAnnotation(YamlList.class);
                Class<?> valueClass = yamlList.value();
                declaredMethod.invoke(instance, param.getList(yamlList.name(), valueClass));
            } else if (declaredMethod.isAnnotationPresent(YamlKey.class)) {
                declaredMethod.invoke(instance, param.getString("====configName"));
            }
        }
        return instance;
    }

    public static ConfigurationSection copy(ConfigurationSection value) {
        YamlConfiguration configuration = new YamlConfiguration();
        for (String key : configuration.getKeys(false)) {
            if (configuration.isConfigurationSection(key)) {
                configuration.set(key, copy(configuration.getConfigurationSection(key)));
            } else {
                configuration.set(key, configuration.get(key));
            }
        }
        return configuration;
    }

    public static String path0(String value, Member member) {
        return !"".equals(value) ? value : member.getName();
    }
}
