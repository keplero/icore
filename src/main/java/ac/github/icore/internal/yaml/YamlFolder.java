package ac.github.icore.internal.yaml;

import ac.github.icore.internal.util.FileUtils;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class YamlFolder {

    public String name;
    public Plugin plugin;
    public boolean mkdirs;
    public File folderFile;

    public YamlFolder(String name, Plugin plugin, boolean mkdirs) {
        this.name = name;
        this.plugin = plugin;
        this.mkdirs = mkdirs;
        folderFile = new File(plugin.getDataFolder(), name);
    }

    public YamlFolder(String name, Plugin plugin) {
        this(name, plugin, false);
    }

    public void mkdirs() {
        if (!folderFile.exists()) {
            folderFile.mkdirs();
        }
    }

    public List<YamlConfiguration> listYamlConfigs() {
        List<YamlConfiguration> configs = new ArrayList<>();
        for (File listFile : FileUtils.listFiles(folderFile)) {
            configs.add(YamlConfiguration.loadConfiguration(listFile));
        }
        return configs;
    }

}
