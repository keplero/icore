package ac.github.icore.internal.yaml;

import org.bukkit.command.CommandSender;

public interface MessageOperation {

    void toMsg(CommandSender sender, String path, Object... params);

    String getMsg(String path,Object... params);

    String replaceMsg(String msg,Object... params);

}
