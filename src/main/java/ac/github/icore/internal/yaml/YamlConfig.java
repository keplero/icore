package ac.github.icore.internal.yaml;

import ac.github.icore.internal.util.Utils;
import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

@Getter
public class YamlConfig implements MessageOperation{

    private String filename;
    private File file;
    private YamlConfiguration config;
    private Plugin plugin;

    public YamlConfig(String filename, Plugin plugin) {
        this.filename = filename;
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(),filename);
    }

    public YamlConfig(File file, Plugin plugin) {
        this.filename = file.getName();
        this.file = file;
        this.plugin = plugin;
    }

    public void onLoad(boolean byResources){
        if (!file.exists()){
            if (byResources){
                plugin.saveResource(filename,false);
            }else {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        loadConfig();
    }

    public void loadConfig(){
        this.config = YamlConfiguration.loadConfiguration(file);
    }


    public void onReload(){
        this.onLoad(true);
    }

    public void save(){
        try {
            this.config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public void setConfig(YamlConfiguration config) {
        this.config = config;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void toMsg(CommandSender sender, String path, Object... params) {
        sender.sendMessage(getMsg(path,params));
    }

    @Override
    public String getMsg(String path, Object... params) {
        return replaceMsg(config.getString(path),params);
    }

    @Override
    public String replaceMsg(String msg, Object... params) {
        return Utils.replaceString(msg,params);
    }
}
