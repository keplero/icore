package ac.github.icore.internal.socket;

import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;

public abstract class BSocketServer extends WebSocketServer {

    public int port;

    public BSocketServer(int port) {
        super(new InetSocketAddress(port));
        this.port = port;
    }

}
