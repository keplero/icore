package ac.github.icore.internal.command;

import org.bukkit.command.CommandSender;

public abstract class SubCommandAdapter implements Comparable<SubCommandAdapter> {

    private String command;
    private String argsString;
    private String description;
    private double priority;

    public SubCommandAdapter() {
    }

    public SubCommandAdapter(String command, String argsString, String description, double priority) {
        this.command = command;
        this.argsString = argsString;
        this.description = description;
        this.priority = priority;
    }

    public abstract boolean execute(CommandSender sender, String[] args);

    @Override
    public int compareTo(SubCommandAdapter cmd) {
        return Double.compare(priority, cmd.priority);
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getArgsString() {
        return argsString;
    }

    public void setArgsString(String argsString) {
        this.argsString = argsString;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }
}
