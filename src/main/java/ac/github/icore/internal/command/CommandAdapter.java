package ac.github.icore.internal.command;

import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.util.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class CommandAdapter implements CommandExecutor {

    public Plugin plugin;
    public CommandAdapterMessage message;
    public String command;

    public CommandAdapter(Plugin plugin, CommandAdapterMessage message, String command) {
        this.plugin = plugin;
        this.message = message;
        this.command = command;
    }

    public CommandAdapter(String command) {
        this.command = command;
    }

    public CommandAdapter(Plugin plugin, CommandAdapterMessage message) {
        this.plugin = plugin;
        this.message = message;
    }

    public CommandAdapter(Plugin plugin, String command) {
        this.plugin = plugin;
        this.command = command;
        this.message = new CommandAdapterMessage(plugin.getConfig());
    }

    public CommandAdapter() {
    }


    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
        this.message = new CommandAdapterMessage(plugin.getConfig());
    }

    public List<SubCommandAdapter> subCommandAdapters = new ArrayList<>();

    public CommandAdapter registerSub(SubCommandAdapter... commandAdapters) {
        for (SubCommandAdapter adapter : commandAdapters) {
            List<SubCommandAdapter> collect = subCommandAdapters.stream()
                    .filter(subCommandAdapter -> subCommandAdapter.getCommand().equalsIgnoreCase(adapter.getCommand()))
                    .collect(Collectors.toList());
            if (collect.size() != 0) {
                collect.forEach(subCommandAdapter -> subCommandAdapters.remove(subCommandAdapter));
            }
            this.subCommandAdapters.add(adapter);
        }
        Collections.sort(subCommandAdapters);
        return this;
    }

    public Optional<SubCommandAdapter> getCommand(String command) {
        return subCommandAdapters
                .stream().filter(commandSub -> commandSub.getCommand().equalsIgnoreCase(command)).findFirst();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args.length != 0) {

            Optional<SubCommandAdapter> optional = getCommand(args[0]);
            if (optional.isPresent()) {

                SubCommandAdapter commandAdapter = optional.get();
                if (isUse(sender, commandAdapter)) {
                    return commandAdapter.execute(sender, args);
                } else {
                    sender.sendMessage("§c无权执行命令.");
                }
            } else {
                sender.sendMessage("§c无效的命令.");
            }

        } else {
            helpers(sender);
        }

        return false;
    }

    public void helpers(CommandSender sender) {

        if (plugin.getConfig().isString("CommandFormat.Prefix")) {
            sender.sendMessage(plugin.getConfig().getString("CommandFormat.Prefix"));
        }

        subCommandAdapters.forEach(subCommandAdapter -> helper(sender, subCommandAdapter));

        if (plugin.getConfig().isString("CommandFormat.Suffix")) {
            sender.sendMessage(plugin.getConfig().getString("CommandFormat.Suffix"));
        }
    }

    public String getPermission(SubCommandAdapter adapter) {
        return plugin.getDescription().getName() + "." + adapter.getCommand().toLowerCase();
    }

    public boolean isUse(CommandSender sender, SubCommandAdapter adapter) {
        return sender.hasPermission(getPermission(adapter));
    }

    public void helper(CommandSender sender, SubCommandAdapter baseCommand) {
        if (isUse(sender, baseCommand)) {

            String string;
            if (plugin.getConfig().isString("CommandFormat.Sub")) {
                string = plugin.getConfig().getString("CommandFormat.Sub")
                        .replace("{command}", baseCommand.getCommand())
                        .replace("{argsCommand}", baseCommand.getArgsString() != null ? baseCommand.getArgsString() : "")
                        .replace("{description}", baseCommand.getDescription());
            } else {
                StringBuilder builder = new StringBuilder("§b/").append(command).append(" ");
                builder.append(baseCommand.getCommand());
                if (baseCommand.getArgsString() != null && !baseCommand.getArgsString().isEmpty()) {
                    builder.append(" ").append(baseCommand.getArgsString());
                }
                builder.append(" ").append(baseCommand.getDescription());
                string = builder.toString();
            }

            sender.sendMessage(string);
        }
    }

    public static class CommandAdapterMessage {

        public String NOT_FOUND_SUBCOMMAND = "无效的命令";
        public String NOT_PERMISSION_EXECUTE = "无权执行指令";

        private FileConfiguration configuration;
        public boolean enableYamlConfig;


        public CommandAdapterMessage() {
            this(null, false);
        }


        public CommandAdapterMessage(FileConfiguration configuration) {
            this(configuration, true);
        }


        public CommandAdapterMessage(FileConfiguration configuration, boolean enableYamlConfig) {
            this.configuration = configuration;
            this.enableYamlConfig = enableYamlConfig;
        }

        public void sendSender(CommandSender sender, String msg) {
            this.sendSender(sender, msg, Param.newInstance());
        }

        public void sendSender(CommandSender sender, String msg, Param param) {
            if (enableYamlConfig) {
                sendSenderToYamlConfig(sender, msg, param);
            } else {
                sender.sendMessage(Utils.replace(msg, param));
            }
        }

        public void sendSenderToYamlConfig(CommandSender sender, String path) {
            sendSenderToYamlConfig(sender, path, Param.newInstance());
        }

        public void sendSenderToYamlConfig(CommandSender sender, String path, Param param) {
            sender.sendMessage(Utils.replace(configuration.getString(path), param));
        }

    }
}