package ac.github.icore.internal.conversion;

import ac.github.icore.internal.util.ClassUtil;

import java.util.ArrayList;
import java.util.List;

public class Conversions {

    public static List<ObjectConversion<?>> conversions = new ArrayList<>();

    static {
        conversions.add(new BooleanConversion());
        conversions.add(new DoubleConversion());
        conversions.add(new FloatConversion());
        conversions.add(new IntegerConversion());
        conversions.add(new LongConversion());
        conversions.add(new ShortConversion());
        conversions.add(new ParamConversion());
    }

    @SuppressWarnings("unchecked")
    public static <T> T covert(Object value, Class<?> c) {
        for (ObjectConversion<?> objectConversion : conversions) {
            Class<?> classT = ClassUtil.getInterfaceT(objectConversion, 0);
            if (classT.equals(c)) {
                return (T) objectConversion.covert(value);
            }
        }
        return (T) value;
    }

}
