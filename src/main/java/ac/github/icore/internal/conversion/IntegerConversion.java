package ac.github.icore.internal.conversion;

public class IntegerConversion implements ObjectConversion<Integer> {
    @Override
    public Integer covert(Object value) {
        return Integer.parseInt(value.toString());
    }
}
