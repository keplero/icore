package ac.github.icore.internal.conversion;

public interface ObjectConversion<T> {

    T covert(Object value);

    default T newInstance(Object value) {
        return covert(value);
    }

}
