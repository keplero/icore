package ac.github.icore.internal.conversion;

public class BooleanConversion implements ObjectConversion<Boolean> {
    @Override
    public Boolean covert(Object value) {
        return Boolean.parseBoolean(value.toString());
    }

}
