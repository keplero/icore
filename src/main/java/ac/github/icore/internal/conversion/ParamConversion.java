package ac.github.icore.internal.conversion;

import ac.github.icore.internal.container.Param;

import java.util.Map;

public class ParamConversion implements ObjectConversion<Param> {

    @Override
    public Param covert(Object value) {
        if (value instanceof Map) {
            return Param.of((Map<String, Object>) value);
        }
        return null;
    }
}
