package ac.github.icore.internal.conversion;

public class LongConversion implements ObjectConversion<Long>{
    @Override
    public Long covert(Object value) {
        return Long.parseLong(value.toString());
    }
}
