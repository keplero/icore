package ac.github.icore.internal.conversion;

public class ShortConversion implements ObjectConversion<Short> {
    @Override
    public Short covert(Object value) {
        return Short.parseShort(value.toString());
    }
}
