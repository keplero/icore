package ac.github.icore.internal.conversion;

public class FloatConversion implements ObjectConversion<Float> {
    @Override
    public Float covert(Object value) {
        return Float.parseFloat(value.toString());
    }
}
