package ac.github.icore.internal.conversion;

public class DoubleConversion implements ObjectConversion<Double> {
    @Override
    public Double covert(Object value) {
        return Double.parseDouble(value.toString());
    }

}
