package ac.github.icore.internal.chest;

public class IconBlock {

    public Character character;
    public int index;
    public int internalIndex;
    public ChestIconHolder holder;

    public IconBlock(Character character, int index, int internalIndex, ChestIconHolder holder) {
        this.character = character;
        this.index = index;
        this.internalIndex = internalIndex;
        this.holder = holder;
    }


}
