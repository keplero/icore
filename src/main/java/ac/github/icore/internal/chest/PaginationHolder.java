package ac.github.icore.internal.chest;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.List;

public abstract class PaginationHolder implements InventoryHolder {

    public List<? extends ChestIconHolder> source;

    public int current;
    public int total;
    public int pageSize;
    public int pageTotal;

    public List<? extends ChestIconHolder> currentSource;


    public PaginationHolder(List<? extends ChestIconHolder> source, int current, int pageSize) {
        pageSize(pageSize).current(current);
        this.source(source);
    }

    public PaginationHolder source(List<? extends ChestIconHolder> source) {
        this.source = source;
        return calculation();
    }

    public PaginationHolder calculation() {
        double v = ((double) source.size()) / pageSize;
        pageTotal = (int) Math.ceil(v);
        total = source.size();
        return this;
    }

    public PaginationHolder current(int current) {
        this.current = current;
        if (source != null && !source.isEmpty()) {
            this.currentSource = get(current);
        }
        return this;
    }

    public List<? extends ChestIconHolder> getCurrentSource() {
        if (currentSource == null) {
            currentSource = get(current);
        }
        return currentSource;
    }

    public List<? extends ChestIconHolder> get(int current) {
        int start = (current - 1) * pageSize;
        int end = Math.min(source.size(), start + pageSize);
        return source.subList(start, end);
    }

    public List<? extends ChestIconHolder> get() {
        return this.currentSource;
    }

    public PaginationHolder pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }


    @Override
    public Inventory getInventory() {
        return null;
    }
}
