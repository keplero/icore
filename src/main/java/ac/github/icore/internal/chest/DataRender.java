package ac.github.icore.internal.chest;


public class DataRender {

    public int index;
    public Character character;


    public DataRender(int index, Character character) {
        this.index = index;
        this.character = character;
    }
}
