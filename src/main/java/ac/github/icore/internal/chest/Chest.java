package ac.github.icore.internal.chest;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.util.Consumer;

import java.util.*;
import java.util.function.Function;

public class Chest {


    public static List<Chest> chests = new ArrayList<>();

    public Inventory inventory;
    public String name;
    public int row;
    public int col;
    public InventoryHolder holder;
    public char[][] layout;
    public Map<Character, Map<Integer, IconBlock>> map = new HashMap<>();
    public List<Consumer<ClickEvent>> clicks = new ArrayList<>();
    public List<Consumer<InventoryCloseEvent>> closes = new ArrayList<>();


    public static Optional<Chest> find(Event event, boolean filterName) {

        if (event instanceof InventoryEvent) {

            InventoryEvent inventoryEvent = (InventoryEvent) event;

            if (filterName) {
                String name = inventoryEvent.getView().getTitle();
                return chests.stream().filter(chest -> chest.name.equalsIgnoreCase(name))
                        .findFirst();
            } else {
                InventoryHolder holder = inventoryEvent.getInventory().getHolder();
                if (holder == null) {
                    return find(event, true);
                } else {
                    return chests.stream().filter(chest -> chest.holder.equals(holder))
                            .findFirst();
                }
            }
        }
        return Optional.empty();
    }


    public static class Builder {

        public Chest chest;

        public Builder() {
            this.chest = new Chest();
        }

        public Builder name(String name) {
            chest.name = name;
            return this;
        }


        public Builder size(int row, int col) {
            return row(row).col(col).draw();
        }

        public Builder row(int row) {
            chest.row = row;
            return this;
        }

        public Builder col(int col) {
            chest.col = col;
            return this;
        }

        public Builder holder(InventoryHolder holder) {
            chest.holder = holder;
            return this;
        }

        public Builder layout(String... strings) {
            for (int i = 0; i < chest.layout.length; i++) {
                String string = strings[i];
                for (int i1 = 0; i1 < chest.layout[i].length; i1++) {
                    chest.layout[i][i1] = string.charAt(i1);
                }
            }
            return this;
        }

        public Builder draw() {
            chest.layout = new char[chest.row][9];
            return this;
        }

        public Builder render(Function<Character, ChestIconHolder> function) {
            char[][] layout = chest.layout;
            for (int i = 0; i < layout.length; i++) {
                char[] chars = layout[i];
                for (int i1 = 0; i1 < chars.length; i1++) {
                    char aChar = chars[i1];
                    int index = i * 9 + i1;
                    if (!chest.map.containsKey(aChar)) {
                        chest.map.put(aChar, new HashMap<>());
                    }
                    Map<Integer, IconBlock> map = chest.map.get(aChar);
                    int size = map.size();

                    map.put(size, new IconBlock(aChar, index, size, function.apply(aChar)));
                }
            }
            return this;
        }

        public Builder click(Consumer<ClickEvent> consumer) {
            chest.clicks.add(consumer);
            return this;
        }


        public Builder close(Consumer<InventoryCloseEvent> consumer) {
            chest.closes.add(consumer);
            return this;
        }

        public Inventory build(Consumer<Inventory> consumer) {
            Inventory inventory = Bukkit.createInventory(chest.holder, chest.row * chest.col, chest.name);
            char[][] layout = chest.layout;
            Map<Character, Integer> cache = new HashMap<>();
            for (int i = 0; i < layout.length; i++) {
                char[] chars = layout[i];
                for (int i1 = 0; i1 < chars.length; i1++) {
                    int index = i * 9 + i1;
                    char aChar = chars[i1];
                    if (chest.map.containsKey(aChar)) {
                        Integer aIndex = cache.getOrDefault(aChar, 0);
                        IconBlock iconBlock = chest.map.get(aChar).get(aIndex);
                        inventory.setItem(index, iconBlock.holder.itemStack());
                        cache.put(aChar, aIndex + 1);
                    }
                }
            }
            if (consumer != null) {
                consumer.accept(inventory);
            }
            Chest.chests.add(chest);
            return inventory;
        }

        public Builder open(Player player) {
            Inventory inventory = build();
            player.openInventory(inventory);
            return this;
        }

        public Inventory build() {
            return build(null);
        }
    }
}
