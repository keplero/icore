package ac.github.icore.internal.chest;

import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Map;
import java.util.Optional;

public class ClickEvent {

    public Chest chest;
    public InventoryClickEvent e;


    public ClickEvent(Chest chest, InventoryClickEvent e) {
        this.chest = chest;
        this.e = e;
    }

    @SuppressWarnings("all")
    public Optional<IconBlock> icon() {
        int rawSlot = e.getRawSlot();
        for (Map<Integer, IconBlock> integerIconMap : chest.map.values()) {
            for (IconBlock icon : integerIconMap.values()) {
                if (icon.index == rawSlot) return Optional.of(icon);
            }
        }
        return Optional.empty();
    }

}
