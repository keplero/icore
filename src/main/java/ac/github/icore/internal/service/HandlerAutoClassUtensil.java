package ac.github.icore.internal.service;


import ac.github.icore.internal.service.bean.JavaClassBean;

public interface HandlerAutoClassUtensil {

    void before(Object parent,Class<?> aClass);

    void after(JavaClassBean javaClassBean);

}
