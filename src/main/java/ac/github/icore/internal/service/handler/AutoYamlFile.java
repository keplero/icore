package ac.github.icore.internal.service.handler;

import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.Service;
import ac.github.icore.internal.service.annotation.IFile;
import ac.github.icore.internal.service.bean.JavaClassBean;

import ac.github.icore.internal.yaml.YamlConfig;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

public class AutoYamlFile implements HandlerAutoClassUtensil {
    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {

        Class<?> aClass = javaClassBean.aClass;
        Object value = javaClassBean.value;
        for (Field declaredField : aClass.getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(IFile.class)) {
                IFile annotation = declaredField.getAnnotation(IFile.class);
                String name = annotation.name();
                Class<?> source = annotation.source();
                Object plugin = null;
                if (source == Void.class) {
                    plugin = javaClassBean.parent;
                } else {
                    Optional<JavaClassBean> optional = Service.javaClassBeanMap
                            .values()
                            .stream()
                            .filter(classBean -> classBean.parent.getClass().equals(source))
                            .findFirst();
                    if (optional.isPresent()) {
                        plugin = optional.get().parent;
                    }
                }
                declaredField.setAccessible(true);
                try {
                    if (plugin != null) {
                        Plugin aPlugin = (Plugin) plugin;
                        YamlConfig yamlConfig = new YamlConfig(new File(aPlugin.getDataFolder(), name), aPlugin);
                        declaredField.set(value, yamlConfig);
                        yamlConfig.onLoad(annotation.byResource());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Method declaredMethod : aClass.getDeclaredMethods()) {
            if (declaredMethod.isAnnotationPresent(IFile.class)) {
                IFile annotation = declaredMethod.getAnnotation(IFile.class);
                String name = annotation.name();
                Class<?> source = annotation.source();
                Object plugin = null;
                if (source == Void.class) {
                    plugin = javaClassBean.parent;
                } else {
                    Optional<JavaClassBean> optional = Service.javaClassBeanMap
                            .values()
                            .stream()
                            .filter(classBean -> classBean.parent.getClass().equals(source))
                            .findFirst();
                    if (optional.isPresent()) {
                        plugin = optional.get().parent;
                    }
                }
                declaredMethod.setAccessible(true);
                try {
                    if (plugin != null) {
                        Plugin aPlugin = (Plugin) plugin;
                        YamlConfig yamlConfig = new YamlConfig(new File(aPlugin.getDataFolder(), name), aPlugin);
                        yamlConfig.onLoad(annotation.byResource());
                        try {
                            declaredMethod.invoke(value, yamlConfig);
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
