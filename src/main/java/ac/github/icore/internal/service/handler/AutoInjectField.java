package ac.github.icore.internal.service.handler;


import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.PackageRegistry;
import ac.github.icore.internal.service.Service;
import ac.github.icore.internal.service.annotation.Bean;
import ac.github.icore.internal.service.annotation.Inject;
import ac.github.icore.internal.service.bean.JavaClassBean;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;

public class AutoInjectField implements HandlerAutoClassUtensil {


    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {
        Class<?> c = javaClassBean.aClass;
        Object o = javaClassBean.value;
        if (c.isAnnotationPresent(Inject.class)) {
            Field[] declaredFields = c.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                boolean accessible = declaredField.isAccessible();
                if (declaredField.isAnnotationPresent(Bean.class)) {
                    Bean bean = declaredField.getAnnotation(Bean.class);
                    String name = bean.value();
                    declaredField.setAccessible(false);
                    if (Service.javaClassBeanMap.containsKey(name)) {
                        declaredField.setAccessible(accessible);
                        try {
                            declaredField.set(o, Service.getBean(name, declaredField.getType()));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        PackageRegistry.logger.log(Level.WARNING, "无效的Bean实体 {0}", name);
                    }
                }
            }

            Method[] declaredMethods = c.getDeclaredMethods();
            for (Method declaredMethod : declaredMethods) {
                boolean accessible = declaredMethod.isAccessible();
                if (declaredMethod.isAnnotationPresent(Bean.class)) {
                    Bean bean = declaredMethod.getAnnotation(Bean.class);
                    String name = bean.value();
                    declaredMethod.setAccessible(false);
                    if (Service.javaClassBeanMap.containsKey(name)) {
                        declaredMethod.setAccessible(accessible);
                        try {
                            declaredMethod.invoke(o, Service.getBean(name).value);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    } else {
                        PackageRegistry.logger.log(Level.WARNING, "无效的Bean实体 {0}", name);
                    }
                }
            }


        }
    }
}
