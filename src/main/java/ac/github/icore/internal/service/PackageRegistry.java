package ac.github.icore.internal.service;


import ac.github.icore.internal.service.bean.JavaClassBean;
import ac.github.icore.internal.util.ClassUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PackageRegistry {

    public Object parent;
    public static Logger logger = Logger.getLogger(PackageRegistry.class.getSimpleName());
    public List<Class<?>> classes;


    public PackageRegistry(Object parent) {
        this.parent = parent;
        Class<?> parentClass = parent.getClass();
        classes = ClassUtil.getClasses(parentClass, new String[]{});
        initBefore();
        initAfter();
    }

    public void initBefore() {
        classes.forEach(aClass -> Service.autoClassUtensils.forEach(handlerAutoClassUtensil ->
                handlerAutoClassUtensil.before(parent, aClass)));
    }

    public void initAfter() {
        List<JavaClassBean> list = new ArrayList<>();
        for (Class<?> aClass : classes) {
            JavaClassBean javaClassBean = this.handleBean(aClass);
            if (javaClassBean != null) {
                list.add(javaClassBean);
            }
        }
        for (JavaClassBean bean : list) {
            JavaBeans.inject(bean);
        }
    }


    public JavaClassBean handleBean(Class<?> aClass) {
        return JavaBeans.registerBean(parent, aClass);
    }


}
