package ac.github.icore.internal.service;


import ac.github.icore.internal.service.bean.JavaClassBean;
import ac.github.icore.internal.service.handler.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service {

    public static List<HandlerAutoClassUtensil> autoClassUtensils = new ArrayList<>();
    public static Map<String, JavaClassBean> javaClassBeanMap = new HashMap<>();


    public static Object hasBean(String name) {
        return javaClassBeanMap.containsKey(name);
    }

    public static JavaClassBean getBean(String name) {
        return javaClassBeanMap.get(name);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name, Class<?> aClass) {
        return (T) aClass.cast(getBean(name).value);
    }


    static {
        autoClassUtensils.add(new AutoInjectField());
        autoClassUtensils.add(new BukkitListener());
        autoClassUtensils.add(new BukkitCommand());
        autoClassUtensils.add(new AutoYamlFile());
        autoClassUtensils.add(new AutoYamlFolder());
    }

}
