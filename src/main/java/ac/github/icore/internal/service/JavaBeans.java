package ac.github.icore.internal.service;


import ac.github.icore.internal.service.annotation.Bean;
import ac.github.icore.internal.service.bean.JavaClassBean;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaBeans {

    public static Logger logger = Logger.getLogger("ServiceBean");

    public static final Set<Class<?>> FILTER_ANNOTATION = new HashSet<>(Arrays.asList(
            Target.class, Retention.class, Documented.class
    ));

    public static JavaClassBean registerBean(Object parent, Class<?> aClass) {
        String beanName = getBeanName(aClass, FILTER_ANNOTATION);
        if (beanName != null) {
            return registerBean(parent, beanName, aClass, null);
        }
        return null;
    }

    public static JavaClassBean registerBean(Object parent, String beanName, Class<?> aClass, Object value) {
        return registerBean(beanName, new JavaClassBean(parent, aClass, value));
    }

    public static JavaClassBean registerBean(String beanName, JavaClassBean classBean) {
        Class<?> aClass = classBean.aClass;
        if (classBean.value == null) {
            try {
                classBean.value = newInstance(aClass);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        Service.javaClassBeanMap.put(beanName, classBean);
        System.out.println("Register java bean " + beanName + " class " + classBean.aClass);
        return classBean;
    }

    public static JavaClassBean inject(JavaClassBean classBean) {
        Service.autoClassUtensils.forEach(handlerAutoClassUtensil -> handlerAutoClassUtensil.after(classBean));
        return classBean;
    }


    public static Object newInstance(Class<?> aClass) throws NoSuchFieldException {
        try {
            Constructor<?> constructor = aClass.getConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new NoSuchFieldException(aClass + " 必须提供一个无参构造器");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            System.out.println("aClass = " + aClass);
            e.printStackTrace();
        }
        return null;
    }

    public static String getBeanName(Class<?> aClass) {
        return getBeanName(aClass, new HashSet<>());
    }

    public static String getBeanName(Class<?> aClass, Set<Class<?>> filters) {
        if (aClass.isAnnotationPresent(Bean.class)) {
            Bean annotation = aClass.getDeclaredAnnotation(Bean.class);
            return annotation.value();
        } else {
            Annotation[] declaredAnnotations = aClass.getAnnotations();
            for (Annotation annotation : declaredAnnotations) {
                Class<? extends Annotation> annotationType = annotation.annotationType();
                if (!filters.contains(annotationType)) {

                    String beanName = getBeanName(annotationType, filters);
                    if (beanName != null) {
                        return beanName;
                    }
                }
            }
        }
        return null;
    }
}
