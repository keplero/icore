package ac.github.icore.internal.service.handler;

import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.bean.JavaClassBean;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

public class BukkitListener implements HandlerAutoClassUtensil {


    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {
        Object parent = javaClassBean.parent;
        Object value = javaClassBean.value;
        if (value instanceof Listener && parent instanceof Plugin) {
            Bukkit.getPluginManager().registerEvents((Listener) value, (Plugin) parent);
        }
    }
}
