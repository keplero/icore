package ac.github.icore.internal.service.handler;

import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.Service;
import ac.github.icore.internal.service.annotation.IFolder;
import ac.github.icore.internal.service.bean.JavaClassBean;
import ac.github.icore.internal.yaml.YamlFolder;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

public class AutoYamlFolder implements HandlerAutoClassUtensil {
    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {

        Class<?> aClass = javaClassBean.aClass;
        Object value = javaClassBean.value;
        for (Field declaredField : aClass.getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(IFolder.class)) {
                IFolder annotation = declaredField.getAnnotation(IFolder.class);
                String name = annotation.name();
                Class<?> source = annotation.source();
                Object plugin = null;
                if (source == Void.class) {
                    plugin = javaClassBean.parent;
                } else {
                    Optional<JavaClassBean> optional = Service.javaClassBeanMap
                            .values()
                            .stream()
                            .filter(classBean -> classBean.parent.getClass().equals(source))
                            .findFirst();
                    if (optional.isPresent()) {
                        plugin = optional.get().parent;
                    }
                }
                declaredField.setAccessible(true);
                try {
                    if (plugin != null) {
                        Plugin aPlugin = (Plugin) plugin;
                        YamlFolder folder = new YamlFolder(name, aPlugin, annotation.mkdirs());
                        declaredField.set(value, folder);
                        if (folder.mkdirs && !folder.folderFile.exists()) {
                            folder.mkdirs();
                            beginFolderCreate(annotation,folder);
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Method declaredMethod : aClass.getDeclaredMethods()) {
            if (declaredMethod.isAnnotationPresent(IFolder.class)) {
                IFolder annotation = declaredMethod.getAnnotation(IFolder.class);
                String name = annotation.name();
                Class<?> source = annotation.source();
                Object plugin = null;
                if (source == Void.class) {
                    plugin = javaClassBean.parent;
                } else {
                    Optional<JavaClassBean> optional = Service.javaClassBeanMap
                            .values()
                            .stream()
                            .filter(classBean -> classBean.parent.getClass().equals(source))
                            .findFirst();
                    if (optional.isPresent()) {
                        plugin = optional.get().parent;
                    }
                }
                declaredMethod.setAccessible(true);
                try {
                    if (plugin != null) {
                        Plugin aPlugin = (Plugin) plugin;
                        try {
                            YamlFolder folder = new YamlFolder(name, aPlugin, annotation.mkdirs());
                            declaredMethod.invoke(value, folder);
                            if (folder.mkdirs && !folder.folderFile.exists()) {
                                folder.mkdirs();
                                beginFolderCreate(annotation,folder);
                            }
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void beginFolderCreate(IFolder iFolder,YamlFolder folder) {
        if (iFolder.defaults().length != 0) {
            Plugin plugin = folder.plugin;
            for (String aDefault : iFolder.defaults()) {
                plugin.saveResource(folder.name +"/"+aDefault,false);
            }
        }
    }
}
