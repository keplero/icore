package ac.github.icore.internal.service.bean;

public class JavaClassBean {

    public Object parent;
    public Class<?> aClass;
    public Object value;

    @Override
    public String toString() {
        return "JavaClassBean{" +
                "parent=" + parent +
                ", aClass=" + aClass +
                ", value=" + value +
                '}';
    }

    public JavaClassBean(Object parent, Class<?> aClass) {
        this.parent = parent;
        this.aClass = aClass;
    }

    public JavaClassBean(Object parent, Class<?> aClass, Object value) {
        this.parent = parent;
        this.aClass = aClass;
        this.value = value;
    }
}
