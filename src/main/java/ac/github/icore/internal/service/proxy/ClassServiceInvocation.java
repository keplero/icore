package ac.github.icore.internal.service.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ClassServiceInvocation implements InvocationHandler {

    public Class<?> aClass;
    public Object value;


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object invoke = method.invoke(value, args);
//        ClassProxy.invokePre(classBean, method, args);
//        System.out.println("value = " + value);
//        Object invoke = method.invoke(value, args);

//        ClassProxy.invokePost(classBean, method, args, invoke);
        return invoke;
    }


}
