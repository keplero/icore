package ac.github.icore.internal.service.handler;


import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.annotation.Aspect;
import ac.github.icore.internal.service.bean.JavaClassBean;
import ac.github.icore.internal.service.proxy.ClassProxy;

import java.lang.reflect.Method;

public class ProxyObject implements HandlerAutoClassUtensil {

    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {
        Class<?> aClass = javaClassBean.aClass;
        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.isAnnotationPresent(Aspect.class)) {
                Aspect aspect = declaredMethod.getAnnotation(Aspect.class);
                ClassProxy.ProxyMethod proxyMethod = new ClassProxy.ProxyMethod(aspect, declaredMethod);
                ClassProxy.methods.add(proxyMethod);
            }
        }
    }
}
