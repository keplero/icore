package ac.github.icore.internal.service.proxy;

import ac.github.icore.internal.service.annotation.Aspect;
import ac.github.icore.internal.service.bean.JavaClassBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassProxy {

    public static List<ProxyMethod> methods = new ArrayList<>();

    public static void invokePre(JavaClassBean classBean, Method method, Object[] args) {
        invoke(Type.Pre, method).forEach(proxyMethod -> {
            try {
                proxyMethod.method.invoke(classBean, args);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }

    public static void invokePost(JavaClassBean classBean, Method method, Object[] args, Object result) {
        invoke(Type.Post, method).forEach(proxyMethod -> {
            try {

                Class<?>[] parameterTypes = proxyMethod.method.getParameterTypes();
                if (parameterTypes.length == 3) {
                    proxyMethod.method.invoke(classBean, args, result);
                } else {
                    proxyMethod.method.invoke(classBean, args);
                }

            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }

    public static List<ProxyMethod> invoke(Type type, Method method) {
        return methods.stream()
                .filter(proxyMethod -> contains(proxyMethod.aspect.types(), type))
                .filter(proxyMethod -> proxyMethod.method.getName().equalsIgnoreCase(method.getName()))
                .collect(Collectors.toList());

    }

    public static boolean contains(Object[] objects, Object o) {
        return Arrays.asList(objects).contains(o);
    }

    public JavaClassBean classBean;
    public InvocationHandler invocation;

    public ClassProxy(JavaClassBean javaClassBean) {
        this.classBean = javaClassBean;

        invocation = (proxy, method, args) -> {
            System.out.println("call");
            return method.invoke(classBean.value, args);
        };

        classBean.value = getClassInstance();
    }

    public Object getClassInstance() {

        return Proxy.newProxyInstance(classBean.aClass.getClassLoader(),
                classBean.aClass.getInterfaces(), invocation);
    }

    @Override
    public String toString() {
        return "ClassProxy{" +
                "classBean=" + classBean +
                ", invocation=" + invocation +
                '}';
    }

    public static class ProxyMethod {

        public Aspect aspect;
        public Method method;

        public ProxyMethod(Aspect aspect, Method method) {
            this.aspect = aspect;
            this.method = method;
        }
    }

    public enum Type {
        Pre, Post
    }

}
