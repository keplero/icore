package ac.github.icore.internal.service.annotation;


import ac.github.icore.internal.service.proxy.ClassProxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {


    String bean();

    String method();

    ClassProxy.Type[] types();

}
