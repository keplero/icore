package ac.github.icore.internal.service.handler;

import ac.github.icore.internal.command.CommandAdapter;
import ac.github.icore.internal.command.SubCommandAdapter;
import ac.github.icore.internal.service.HandlerAutoClassUtensil;
import ac.github.icore.internal.service.annotation.RegisterCommand;
import ac.github.icore.internal.service.annotation.SubCommand;
import ac.github.icore.internal.service.bean.JavaClassBean;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;

public class BukkitCommand implements HandlerAutoClassUtensil {

    @Override
    public void before(Object parent, Class<?> aClass) {

    }

    @Override
    public void after(JavaClassBean javaClassBean) {
        Class<?> aClass = javaClassBean.aClass;
        Object value = javaClassBean.value;
        Object parent = javaClassBean.parent;
        if (aClass.isAnnotationPresent(RegisterCommand.class) && value instanceof CommandAdapter && parent instanceof JavaPlugin) {
            JavaPlugin plugin = (JavaPlugin) parent;
            CommandAdapter commandAdapter = (CommandAdapter) value;
            commandAdapter.setPlugin(plugin);
            plugin.getCommand(commandAdapter.command).setExecutor(commandAdapter);
            try {
                initSubCommands(commandAdapter);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }

    public void initSubCommands(CommandAdapter commandAdapter) throws IllegalAccessException {
        Class<? extends CommandAdapter> aClass = commandAdapter.getClass();
        for (Field declaredField : aClass.getDeclaredFields()) {

            declaredField.setAccessible(true);
            Object o = declaredField.get(commandAdapter);
            if (o instanceof SubCommandAdapter && declaredField.isAnnotationPresent(SubCommand.class)) {
                SubCommand annotation = declaredField.getAnnotation(SubCommand.class);
                SubCommandAdapter subCommandAdapter = (SubCommandAdapter) o;
                subCommandAdapter.setCommand(annotation.value());
                subCommandAdapter.setArgsString(annotation.args());
                subCommandAdapter.setPriority(annotation.priority());
                subCommandAdapter.setDescription(annotation.description());
                commandAdapter.registerSub(subCommandAdapter);
            }
        }
    }

}
