package ac.github.icore.internal.service.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IFolder {

    String name();

    Class<?> source() default Void.class;

    boolean mkdirs() default false;

    String[] defaults() default "";
}
