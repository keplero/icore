package ac.github.icore.internal.mybatis;

import org.apache.ibatis.session.SqlSession;

import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.function.Function;

public class MapperAccept<T> {


    public MybatisBase base;
    public Class<?> entityClass;
    public SqlSession sqlSession = null;


    public MapperAccept(MybatisBase base, Class<?> entityClass) {
        this.base = base;
        this.entityClass = entityClass;
        base.getSqlSession().getConfiguration().addMapper(entityClass);
    }

    public void accept(Consumer<T> consumer) {
        Object mapper = sqlSession().getMapper(entityClass);
        consumer.accept((T) mapper);
        commit();
    }

    public <R> R apply(Function<T, R> function) {
        Object mapper = sqlSession().getMapper(entityClass);
        R apply = function.apply((T) mapper);
        commit();
        return apply;
    }

    public SqlSession sqlSession() {
        try {
            if (sqlSession == null || sqlSession.getConnection().isClosed()) {
                sqlSession = base.getSqlSession();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return sqlSession;
    }


    public void close() {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }

    public void commit() {
        if (sqlSession != null) {
            sqlSession.commit();
        }

    }

}
