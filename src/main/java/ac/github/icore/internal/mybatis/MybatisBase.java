package ac.github.icore.internal.mybatis;

import ac.github.icore.internal.container.Param;
import ac.github.icore.internal.util.HikariCPUtils;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;


/**
 * 数据库操作类
 */
public class MybatisBase {
    private HikariDataSource hikariDataSource;
    private JdbcTransactionFactory jdbcTransactionFactory;
    private Environment environment;
    private Configuration configuration;
    private static SqlSessionFactory sqlSessionFactory = null;

    /**
     * 构造方法
     *
     * @param param  传入数据库所需参数
     * @param plugin 插件实例
     */
    public MybatisBase(Param param, JavaPlugin plugin) {
        hikariDataSource = HikariCPUtils.setupConnectPool(param);
        jdbcTransactionFactory = new JdbcTransactionFactory();
        environment = new Environment(plugin.getName(), jdbcTransactionFactory, hikariDataSource);
        configuration = new Configuration(environment);

        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    }

    public MybatisBase(HikariDataSource dataSource, JavaPlugin plugin) {
        hikariDataSource = dataSource;
        jdbcTransactionFactory = new JdbcTransactionFactory();
        environment = new Environment(plugin.getName(), jdbcTransactionFactory, hikariDataSource);
        configuration = new Configuration(environment);

        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    }


    /**
     * 向MyBatis容器中加入mapper映射器
     *
     * @param c mapper接口类
     */
    public void addMapper(Class<?> c) {
        sqlSessionFactory.getConfiguration().addMapper(c);
    }

    /**
     * 批量向MyBatis容器中加入mapper映射器
     *
     * @param classList mapper接口类集合
     */
    public void addMappers(List<Class<?>> classList) {
        for (Class<?> aClass : classList) {
            sqlSessionFactory.getConfiguration().addMapper(aClass);
        }
    }

    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    public HikariDataSource getHikariDataSource() {
        return hikariDataSource;
    }

    public JdbcTransactionFactory getJdbcTransactionFactory() {
        return jdbcTransactionFactory;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
