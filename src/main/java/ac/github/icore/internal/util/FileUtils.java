package ac.github.icore.internal.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {


    public static List<File> listFiles(File file) {
        List<File> list = new ArrayList<>();
        if (file.isDirectory()) {
            for (File f1 : file.listFiles()) {
                list.addAll(listFiles(f1));
            }
        } else {
            list.add(file);
        }
        return list;
    }



    public static long download(String urlString, File target) throws IOException {
        long timeMillis = System.currentTimeMillis();
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        FileOutputStream out = new FileOutputStream(target);
        InputStream ins = con.getInputStream();
        byte[] b = new byte[1024];
        int i = 0;
        while ((i = ins.read(b)) != -1) {
            out.write(b, 0, i);
        }
        ins.close();
        out.close();
        return System.currentTimeMillis() - timeMillis;
    }


    public static void addUrl(File jarPath) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException, MalformedURLException {
        ClassLoader systemClassLoader = FileUtils.class.getClassLoader();
        URL url = jarPath.toURI().toURL();
        if (systemClassLoader instanceof URLClassLoader) {
            URLClassLoader classLoader = (URLClassLoader) systemClassLoader;
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            method.invoke(classLoader, url);
        }

    }


}
