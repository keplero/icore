package ac.github.icore.internal.util;

import java.util.Random;

public class NumberUtils {

    private static Random random;

    public int next(int min,int max){
        return random.nextInt(max - min + 1) + min;
    }

    static {
        random = new Random();
    }


}
