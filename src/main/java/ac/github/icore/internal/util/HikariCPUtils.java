package ac.github.icore.internal.util;

import ac.github.icore.internal.container.Param;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikariCPUtils {


    /**
     * 创建数据源
     * @param param 参数
     * @return dataSource
     */
    public static HikariDataSource setupConnectPool(Param param){
        return setupConnectPool(createHikariConfig(param));
    }


    /**
     * 创建数据源
     * @param hikariConfig config
     * @return dataSource
     */
    public static HikariDataSource setupConnectPool(HikariConfig hikariConfig){
        return new HikariDataSource(hikariConfig);
    }

    /**
     * 创建数据配置
     * @param param param
     *              connectTimeout 连接超时时间
     *              minimumIdle 最小连接数
     *              maximumPoolSize 最大连接数
     *              mysql
     *                ip IP地址
     *                port 端口
     *                username 用户名
     *                password 密码
     *                database 数据库
     *                options 参数
     * @return config
     */
    public static HikariConfig createHikariConfig(Param param){
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("com.mysql.jdbc.Driver");
        hikariConfig.setConnectionTimeout(param.getLong("connectTimeout",30000));
        hikariConfig.setMinimumIdle(param.getInt("minimumIdle",10));
        hikariConfig.setMaximumPoolSize(param.getInt("maximumPoolSize",10));

        Param mysql = param.getParam("mysql");

        StringBuilder jdbcUrl = new StringBuilder("jdbc:mysql://")
                .append(mysql.getString("ip"))
                .append(":")
                .append(mysql.getString("port"))
                .append("/")
                .append(mysql.getString("database"));
        if (mysql.containsKey("options")){
            jdbcUrl.append("?")
                    .append(mysql.getString("options"));
        }
        hikariConfig.setJdbcUrl(jdbcUrl.toString());
        hikariConfig.setUsername(mysql.getString("username"));
        hikariConfig.setPassword(mysql.getString("password"));
        if (mysql.containsKey("configuration")) {
            Param configuration = mysql.getParam("configuration");
            configuration.forEach(hikariConfig::addDataSourceProperty);
        }

        return hikariConfig;
    }

}
