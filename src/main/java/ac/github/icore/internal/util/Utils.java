package ac.github.icore.internal.util;

import ac.github.icore.internal.container.Param;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class Utils {


    public static ItemStack item(String name, List<String> lore, String type, String... def) {
        ItemStack itemStack = MaterialUtil.fromString(type, def);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(name);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack item(ConfigurationSection configurationSection) {
        return item(configurationSection, Param.newInstance());
    }

    public static ItemStack item(ConfigurationSection configurationSection, Param param) {
        ItemStack itemStack = new ItemStack(Material.valueOf(configurationSection.getString("id", "STONE")
                .toUpperCase()));
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(replace(configurationSection.getString("name", " "), param));
        itemMeta.setLore(replaceList(configurationSection.getStringList("lore"), param));
        itemStack.setItemMeta(itemMeta);
        short data = (short) configurationSection.getInt("data", 0);
        itemStack.setDurability(data);
        return itemStack;
    }

    public static String replaceString(String msg, Object... param) {
        for (int i = 0; i < param.length; i++) {
            msg = msg.replace("{" + i + "}", param[i].toString());
        }
        return msg;
    }


    public static Param toParam(ConfigurationSection configurationSection) {

        for (String key : configurationSection.getKeys(true)) {
            Object o = configurationSection.get(key);
        }
        return Param.newInstance();
    }


    public static String replace(String string, Param param) {
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            Object entryValue = entry.getValue();
            String pathKey = "{" + entry.getKey() + "}";
            if (!(entryValue instanceof List)) {
                string = string.replace(pathKey, entryValue.toString());
            } else {
                List<String> list = (List<String>) entryValue;
                List<String> aList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (string.contains(pathKey)) {
                        aList.add(string.replace(pathKey,list.get(i)));
                    }
                }
                if (!aList.isEmpty()) {
                    string = String.join("</r>", aList);
                }
            }

        }
        return string.replace("&", "§");
    }

    public static int evalSize(int total) {
        int row = (int) Math.floor(total / 9);
        int i = total % 9;
        if (i != 0) {
            row += 1;
        }
        return Math.min(row, 5) * 9;
    }

    public static List<String> replaceList(List<String> strings, Param param) {
        if (strings == null) {
            return new ArrayList<>();
        }
        List<String> newList = new ArrayList<>();
        strings.forEach(s -> {
            String replace = replace(s, param);
            if (replace.contains("</r>")) {
                newList.addAll(Arrays.asList(replace.split("</r>")));
            } else {
                newList.add(replace);
            }

        });
        return newList;
    }


    public static Map<String, Object> ofMap(Map<String, Object> objectMap) {
        Map<String, Object> map = new HashMap<>();
        for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
            if (entry.getValue() instanceof MemorySection) {
                map.put(entry.getKey(), Param.of(((MemorySection) entry.getValue()).getValues(false)));
            } else if (entry.getValue() instanceof Param) {
                map.put(entry.getKey(), ofMap((Map<String, Object>) entry.getValue()));
            } else {
                map.put(entry.getKey(), entry.getValue());
            }

        }
        return map;
    }


}
