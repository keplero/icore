package ac.github.icore.internal.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtils {

    public static Object newInstance(Class<?> c,Object... params) throws Exception{
        Class<?>[] types = getTypes(params);
        Constructor<?> declaredConstructor = c.getDeclaredConstructor(types);
        return declaredConstructor.newInstance(params);
    }

    public static Class<?>[] getTypes(Object... objects){
        Class<?>[] objs = new Class<?>[objects.length];
        for (int i = 0; i < objects.length; i++) {
            objs[i] = getType(objects[i]);
        }
        return objs;
    }

    public static Class<?> getType(Object object){
        return object.getClass();
    }

    public static Object invoke(Class<?> c,String string,Object... param)  throws IllegalAccessException,InvocationTargetException,NoSuchMethodException{
        return invoke(c,null,string,param);
    }

    public static Object invoke(Class<?> c,Object object,String string,Object... param)  throws IllegalAccessException,InvocationTargetException,NoSuchMethodException{
        return invoke(object,getMethod(c,string,param),param);
    }

    public static Object invoke(Object object,Method method,Object... param) throws IllegalAccessException,InvocationTargetException{
        return method.invoke(object, param);
    }

    public static Method getMethod(Class<?> c, String string, Object... param) throws NoSuchMethodException{
        return c.getDeclaredMethod(string, getTypes(param));
    }

    public static void main(String[] args) {
        try {



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
