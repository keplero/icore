package ac.github.icore;

import ac.github.icore.internal.handler.OnListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class ICore extends JavaPlugin {

    public static ICore instance;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        Bukkit.getPluginManager().registerEvents(new OnListener(),this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
